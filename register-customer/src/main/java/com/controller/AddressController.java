package com.controller;

import com.documents.Address;
import com.mongodb.client.result.UpdateResult;
import com.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/rest")
public class AddressController  {

    @Autowired
    private MongoTemplate mongoTemplate;

    @PostMapping("/address")
    public Address addAddress(@Valid @RequestBody Address address){
        mongoTemplate.save(address);
        return address;
    }

    @GetMapping("/address")
    public Address findByCity(@RequestParam  String city) {
        Query query = new Query();
        query.addCriteria(Criteria.where("city").is(city));
        Address address = mongoTemplate.findOne(query, Address.class);
        address.setDistrict("Gauteng");
        mongoTemplate.save(address);
        return address;
    }
}
