package com.controller;


import com.component.CustomerMessages;
import com.documents.Customer;
import com.documents.Person;
import com.exception.BusinessException;
import com.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/rest")
public class CustomerController {

    @Autowired
    private CustomerMessages messages;

    @Autowired
    private MongoTemplate mongoTemplate;


    @PostMapping("/customer")
    public Customer createCustomer(@Valid @RequestBody Customer customer){
        System.out.println("Add new User");
        List<Customer> customers = mongoTemplate.findAll(Customer.class);
        boolean isCustomerExist = customers.stream()
                                    .map(c -> c.getPerson().getEmail())
                                    .anyMatch(customer.getPerson().getEmail()::equalsIgnoreCase);
        if (isCustomerExist){
            throw new BusinessException(Constants.CUSTOMER_EXIST, messages.getCustomerExists());
        }
        mongoTemplate.save(customer);
        return customer;
    }

    @GetMapping("/customers")
    public List<Customer> findAllCustomers() {
        System.out.println("Find All Customers");
        return mongoTemplate.findAll(Customer.class);
    }

    @PutMapping("/update/customer")
    public Customer updateCustomerByEmail(@RequestParam  String email, @RequestParam String phone){
        Query updateQuery = new Query();
        updateQuery.addCriteria(Criteria.where("person.email").is(email));
        Customer customer1 = mongoTemplate.findOne(updateQuery, Customer.class);
        Person person = customer1.getPerson();
        person.setPhone(phone);
        customer1.setPerson(person);
        return customer1;
    }
}
