package com.documents;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "address")
public class Address {

    private String district;
    private String city;
    private String postalCode;

    public Address() {
    }

    public Address(String district, String city, String postalCode) {
        this.district = district;
        this.city = city;
        this.postalCode = postalCode;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "district='" + district + '\'' +
                ", city='" + city + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }
}
