package com.documents;


import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Vehicle {

    private String name;
    private String registration;

    public Vehicle() {
    }

    public Vehicle(String name, String registration) {
        this.name = name;
        this.registration = registration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }
}
