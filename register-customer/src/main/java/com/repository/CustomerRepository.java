package com.repository;

import com.documents.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface CustomerRepository extends MongoRepository<Customer, String> {

    @Query("{'customer.person.email': ?0}")
    Customer findByEmail(String email);
}
