package com;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket postsApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("public-api").apiInfo(apiInfo()).select().paths(postPaths()).build();
    }

    private Predicate<String> postPaths(){
        return Predicates.or(regex("/carwash/api.*"), regex("/carwash/api.*"));
    }

    private ApiInfo apiInfo(){
        return new ApiInfoBuilder().title("JavaInUse API")
                .description("Java in use")
                .termsOfServiceUrl("http://javainuse.com")
                .contact("Javainuse").license("Javainuse")
                .licenseUrl("jkgoroba@gmail.com").version("1.0").build();
    }
}
