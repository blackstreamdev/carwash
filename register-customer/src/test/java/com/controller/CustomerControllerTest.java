package com.controller;

import com.component.CustomerMessages;
import com.documents.Address;
import com.documents.Customer;
import com.documents.Person;
import com.documents.Vehicle;
import com.exception.BusinessException;
import com.repository.CustomerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CustomerControllerTest {

    public static final String CUSTOMER_ALREADY_EXISTS = "Customer already exists.";
    public static final String EMAIL = "aaa@com.hh";
    public static final String NAME = "Jerry";
    public static final String CITY = "Benoni";
    public static final String VEHICLE_NAME = "Ford Figo";
    public static final String VEHICLE_REGISTRATION = "FB 33 GP";
    @InjectMocks
    private CustomerController customerController;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private CustomerMessages customerMessages;

    @Test
    public void testShouldSuccessfullyCreateACustomer() throws Exception{
        Customer customer = createCustomerSuccess();
        when(customerRepository.findAll()).thenReturn(new ArrayList<>());
        when(customerRepository.save(customer)).thenReturn(customer);
        Customer actual = customerController.createCustomer(customer);
        assertNotNull(actual);
        assertEquals(EMAIL, actual.getPerson().getEmail());
        assertEquals(NAME, actual.getPerson().getName());
        assertEquals(CITY, actual.getPerson().getAddress().getCity());
        assertEquals(VEHICLE_NAME, actual.getVehicle().getName());
        assertEquals(VEHICLE_REGISTRATION, actual.getVehicle().getRegistration());
    }

    @Test(expected = BusinessException.class)
    public void testShouldThrowBusinessExceptionWhenCustomerAlreadyExist() throws Exception {
        when(customerRepository.findAll()).thenReturn(findCustomers());
        when(customerMessages.getCustomerExists()).thenReturn(CUSTOMER_ALREADY_EXISTS);
        try{
            customerController.createCustomer(createCustomerSuccess());
        }catch (BusinessException e){
            assertEquals(CUSTOMER_ALREADY_EXISTS, e.getMessage());
            assertEquals(new Integer(409), e.getCode());
            throw e;
        }
    }

    @Test
    public void findAllCustomers() {
        when(customerRepository.findAll()).thenReturn(findCustomers());
        List<Customer> actual = customerController.findAllCustomers();
        assertNotNull(actual);
        assertEquals(1, actual.size());
    }

    public List<Customer> findCustomers() {
        List<Customer> customers = new ArrayList<>();
        Person person = new Person();
        Address address = new Address();
        address.setCity("Benoni");
        address.setDistrict("Gauteng");
        address.setPostalCode("1632");
        person.setAddress(address);
        person.setEmail("aaa@com.hh");
        person.setPassword("pass@1");
        person.setPhone("0112552632");
        person.setName("Jerry");
        person.setSurname("Kgoroba");
        Customer customer = new Customer();
        customer.setPerson(person);
        Vehicle vehicle = new Vehicle();
        vehicle.setName("Ford Figo");
        vehicle.setRegistration("FB 33 GP");
        customer.setVehicle(vehicle);
        customers.add(customer);
        return customers;
    }

    public Customer createCustomerSuccess() {
        Person person = new Person();
        Address address = new Address();
        address.setCity("Benoni");
        address.setDistrict("Gauteng");
        address.setPostalCode("1632");
        person.setAddress(address);
        person.setEmail("aaa@com.hh");
        person.setPassword("pass@1");
        person.setPhone("0112552632");
        person.setName("Jerry");
        person.setSurname("Kgoroba");
        Customer customer = new Customer();
        customer.setPerson(person);
        Vehicle vehicle = new Vehicle();
        vehicle.setName("Ford Figo");
        vehicle.setRegistration("FB 33 GP");
        customer.setVehicle(vehicle);
        return customer;
    }
}